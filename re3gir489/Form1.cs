﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using SharpMik.Player;
using SharpMik.Drivers;

namespace re3gir489
{
    public partial class Form1 : Form
    {
        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId);
        [DllImport("kernel32.dll")]
        public static extern bool ReadProcessMemory(int hProcess, int lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead);
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool WriteProcessMemory(int hProcess, int lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesWritten);

        IntPtr processHandle;
        MikMod m_Player = new MikMod();
        SharpMik.Module m_Mod;
        int StartingAddressItem;
        int StartingAddressValue;
        int StartingAddressDisplay;
        int addressOfInventoryPointer;
        IntPtr ReloadByte;
        IntPtr InkRibbonAddress;
        Stream _imageStream = null;
        GlobalKeyboardHook gHook;
        String music;
        Dictionary<byte, string> itemList = new Dictionary<byte, string> {
                {0, "Nothing"},
                {1, "Knife"},
                {2, "Merc's Handgun"},
                {3, "Hand Gun"},
                {4, "Shotgun"},
                {5, "Magnum"},
                {6, "G. Launcher (Burst)"},
                {7, "G. Launcher (Flame)"},
                {8, "G. Launcher (Acid)"},
                {9, "G. Launcher (Freeze)"},
                {10, "Rocket Launcher"},
                {11, "Gatling Gun"},
                {12, "Mine Thrower"},
                {13, "EAGLE 6.0"},
                {14, "Assault Rifle (Manual)"},
                {15, "Assault Rifle (Auto)"},
                {16, "Western Custom"},
                {17, "SIGPRO E"},
                {18, "M92F E"},
                {19, "Benelli M3S E"},
                {20, "M. Thrower E"},
                {21, "H. Gun Bullets"},
                {22, "Magnum Bullets"},
                {23, "Shotgun Shells"},
                {24, "Granade Rounds"},
                {25, "Flame Rounds"},
                {26, "Acid Rounds"},
                {27, "Freeze Rounds"},
                {28, "M.T. Rounds"},
                {29, "A.R. Bullets"},
                {30, "H.G. Bullets E"},
                {31, "S.G. Shell E"},
                {32, "F. Aid Spray"},
                {33, "Green Herb"},
                {34, "Blue Herb"},
                {35, "Red Herb"},
                {36, "Mixed Herb (G+G)"},
                {37, "Mixed Herb (B+G)"},
                {38, "Mixed Herb (R+G)"},
                {39, "Mixed Herb (G+G+G)"},
                {40, "Mixed Herb (G+G+B)"},
                {41, "Mixed Herb (G+R+B)"},
                {42, "F. Aid Box"},
                {43, "Square Crank"},
                {47, "S.T.A.R.S Card (Jill's)"},
                {49, "Battery"},
                {50, "Fire Hook"},
                {51, "Power Cable"},
                {52, "Fuse"},
                {54, "Oil Additive"},
                {55, "Card Case"},
                {56, "S.T.A.R.S Card (Brad's)"},
                {57, "Machine Oil"},
                {58, "Mixed Oil"},
                {60, "Wrench"},
                {61, "Iron Pipe"},
                {63, "Fire Hose"},
                {64, "Tape Recorder"},
                {65, "Lighter Oil"},
                {66, "Empty Lighter"},
                {67, "Lighter"},
                {68, "Green Gem"},
                {69, "Blue Gem"},
                {70, "Amber Ball"},
                {71, "Obsidian Ball"},
                {72, "Crystal Ball"},
                {76, "Gold Gear"},
                {77, "Silver Gear"},
                {78, "Chronos Gear (Silver + Gold Gear)"},
                {79, "Bronze Book"},
                {80, "Bronze Compass"},
                {81, "Vaccine Medium"},
                {82, "Vaccine Base"},
                {85, "Vaccine"},
                {88, "Medium Base"},
                {89, "EAGLE Parts A"},
                {90, "EAGLE Parts B"},
                {91, "M37 Parts A"},
                {92, "M37 Parts B"},
                {94, "Chronos Chain"},
                {95, "Rusted Crank"},
                {96, "Card Key"},
                {97, "Gun Powder A"},
                {98, "Gun Powder B"},
                {99, "Gun Powder C"},
                {100, "Gun Powder AA"},
                {101, "Gun Powder BB"},
                {102, "Gun Powder AC"},
                {103, "Gun Powder BC"},
                {104, "Gun Powder CC"},
                {105, "Gun Powder AAA"},
                {106, "Gun Powder AAB"},
                {107, "Gun Powder BBA"},
                {108, "Gun Powder BBB"},
                {109, "Gun Powder CCC"},
                {110, "Inf. Bullets"},
                {111, "Water Sample"},
                {112, "System Disk"},
                {113, "Dummy Key"},
                {114, "Lockpick"},
                {115, "Warehouse Key"},
                {116, "Sickroom Key"},
                {117, "Emblem Key"},
                {119, "Bezel Key"},
                {120, "Winder Key"},
                {121, "Chronos Key"},
                {123, "Main Gate Key"},
                {124, "Graveyard Key"},
                {125, "Rear Gate Key"},
                {126, "Facility Key"},
                {127, "Facility Key (w/Barcode)"},
                {128, "Boutique Key"},
                {129, "Ink Ribbon"},
                {130, "Reloading Tool"},
                {131, "Game Inst. A"},
                {132, "Game Inst. B"} };
        Dictionary<byte, string> displayList = new Dictionary<byte, string> {
                {0, "Nothing"},
                {1, "Green"},
                {2, "Green Percentage"},
                {3, "Green Infinite"},
                {5, "Red"},
                {6, "Red Percentage"},
                {7, "Red Infinite"},
                {9, "Yellow"},
                {10, "Yellow Percentage"},
                {11, "Yellow Infinite"},
                {13, "Blue"},
                {14, "Blue Percentage"},
                {15, "Blue Infinite"} };

        public Form1()
        {
            var exists = System.Diagnostics.Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Count() > 1;
            if (exists)
            {
                MessageBox.Show("An instance of Resident Evil 3 Trainer is already running.");
                Close();
            }
            InitializeComponent();
            InitalizeForm1();
        }

        private void HandleItemComboBox(ComboBox itemBox)
        {
            itemBox.SelectedIndexChanged -= ItemIndexChanged;
            if (itemBox.DataSource == null)
            {
                itemBox.DataSource = new BindingSource(itemList, null);
                itemBox.DisplayMember = "Value";
                itemBox.ValueMember = "Key";
            }
            itemBox.SelectedIndex = 0;
            itemBox.Enabled = false;
            itemBox.SelectedIndexChanged += ItemIndexChanged;
        }

        private void HandleValueTextBox(TextBox valueBox)
        {
            valueBox.TextChanged -= ValueChanged;
            valueBox.Text = "0";
            valueBox.Enabled = false;
            valueBox.TextChanged += ValueChanged;
        }

        private void HandleDisplayComboBox(ComboBox displayBox)
        {
            displayBox.SelectedIndexChanged -= DisplayIndexChanged;
            if (displayBox.DataSource == null)
            {
                displayBox.DataSource = new BindingSource(displayList, null);
                displayBox.DisplayMember = "Value";
                displayBox.ValueMember = "Key";
            }
            displayBox.SelectedIndex = 0;
            displayBox.Enabled = false;
            displayBox.SelectedIndexChanged += DisplayIndexChanged;
        }

        private void InitalizeForm1()
        {
            HandleItemComboBox(cbxSlot1Item);
            HandleItemComboBox(cbxSlot2Item);
            HandleItemComboBox(cbxSlot3Item);
            HandleItemComboBox(cbxSlot4Item);
            HandleItemComboBox(cbxSlot5Item);
            HandleItemComboBox(cbxSlot6Item);
            HandleItemComboBox(cbxSlot7Item);
            HandleItemComboBox(cbxSlot8Item);
            HandleItemComboBox(cbxSlot9Item);
            HandleItemComboBox(cbxSlot10Item);
            HandleValueTextBox(txtSlot1Value);
            HandleValueTextBox(txtSlot2Value);
            HandleValueTextBox(txtSlot3Value);
            HandleValueTextBox(txtSlot4Value);
            HandleValueTextBox(txtSlot5Value);
            HandleValueTextBox(txtSlot6Value);
            HandleValueTextBox(txtSlot7Value);
            HandleValueTextBox(txtSlot8Value);
            HandleValueTextBox(txtSlot9Value);
            HandleValueTextBox(txtSlot10Value);
            HandleDisplayComboBox(cbxSlot1Display);
            HandleDisplayComboBox(cbxSlot2Display);
            HandleDisplayComboBox(cbxSlot3Display);
            HandleDisplayComboBox(cbxSlot4Display);
            HandleDisplayComboBox(cbxSlot5Display);
            HandleDisplayComboBox(cbxSlot6Display);
            HandleDisplayComboBox(cbxSlot7Display);
            HandleDisplayComboBox(cbxSlot8Display);
            HandleDisplayComboBox(cbxSlot9Display);
            HandleDisplayComboBox(cbxSlot10Display);
            cbkRReloads.CheckedChanged -= ReloadCheckbox_CheckedChanged;
            cbxInkRibbon.CheckedChanged -= InkRibbonCheckbox_CheckedChanged;
            cbkRReloads.Checked = false;
            cbxInkRibbon.Checked = false;
            cbkRReloads.Enabled = false;
            cbxInkRibbon.Enabled = false;
            cbkRReloads.CheckedChanged += ReloadCheckbox_CheckedChanged;
            cbxInkRibbon.CheckedChanged += InkRibbonCheckbox_CheckedChanged;
            StartingAddressItem = 0;
            m_Player.Init<NaudioDriver>("");
            if (gHook == null)
            {
                gHook = new GlobalKeyboardHook();
                gHook.KeyDown += new KeyEventHandler(ReloadKeyDown);
                gHook.KeyUp += new KeyEventHandler(ReloadKeyUp);
                gHook.HookedKeys.Add(Keys.R);
            }
            else
            {
                gHook.UnHook();
            }
            if (_imageStream == null)
            {
                if (new Random().Next(100) < 50)
                {
                    _imageStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("re3gir489.2ND_PM.S3M");
                    music = "Music: 2ND_PM.S3M";
                }
                else
                {
                    _imageStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("re3gir489.arkanoid.xm");
                    music = "Music: arkanoid.xm";
                }
            }
            m_Mod = m_Player.LoadModule(_imageStream);
            m_Player.Play(m_Mod);
            if (music.EndsWith("S3M"))
                m_Mod.volume = 32;
        }

        private void MainTimer_Tick(object sender, EventArgs e)
        {
            Process[] process = Process.GetProcessesByName("ResidentEvil3");
            if (process.Length == 0)
            {
                processHandle = IntPtr.Zero;
                lblWaiting.Text = "Waiting for game";
                lblWaiting.ForeColor = Color.Red;
                process = Process.GetProcessesByName("ResidentEvil3");
                if (m_Player.IsPlaying() == false)
                {
                    InitalizeForm1();
                }
                if (m_Mod.volume < 128)
                    m_Mod.volume += 16;
                return;
            }
            byte[] buffer = new byte[3];
            int bytesRead = 0;
            byte[] addressBuffer = new byte[4];
            if (processHandle == IntPtr.Zero)
            {
                processHandle = OpenProcess(0x38, false, process[0].Id);
                lblWaiting.Text = "Found Game";
                lblWaiting.ForeColor = Color.Green;
                cbkRReloads.Enabled = true;
                cbxInkRibbon.Enabled = true;
                m_Player.Stop();
                SigScan sigScanInstance = new SigScan(process[0], new IntPtr(0x00401000), 0x600000);
                IntPtr signatureLocation = sigScanInstance.FindPattern(new byte[] { 0xA1, 0x00, 0x00, 0x00, 0x00, 0x6A, 0x28 }, "x????xx", 1);
                if (signatureLocation != IntPtr.Zero)
                {
                    ReadProcessMemory((int)processHandle, signatureLocation.ToInt32(), addressBuffer, 4, ref bytesRead);
                    addressOfInventoryPointer = BitConverter.ToInt32(addressBuffer, 0);
                    ReloadByte = sigScanInstance.FindPattern(new byte[] { 0x84, 0xD2, 0x75, 0x06, 0x66, 0x0D, 0xFF, 0xFF }, "xxxxxxxx", 3);
                    InkRibbonAddress = sigScanInstance.FindPattern(new byte[] { 0x7C, 0x15, 0x6A, 0x01, 0x53, 0x53 }, "xxxxxx", 0);
                    if (InkRibbonAddress == IntPtr.Zero)
                        InkRibbonAddress = sigScanInstance.FindPattern(new byte[] { 0x7C, 0x1A, 0x68 }, "xxx", 0); //Timestamp   : 39D81B32 (Mon Oct 02 05:20:50 2000)
                    cbxSlot1Item.Enabled = true;
                    cbxSlot1Item.Enabled = true;
                    cbxSlot2Item.Enabled = true;
                    cbxSlot3Item.Enabled = true;
                    cbxSlot4Item.Enabled = true;
                    cbxSlot5Item.Enabled = true;
                    cbxSlot6Item.Enabled = true;
                    cbxSlot7Item.Enabled = true;
                    cbxSlot8Item.Enabled = true;
                    cbxSlot9Item.Enabled = true;
                    cbxSlot10Item.Enabled = true;
                    txtSlot1Value.Enabled = true;
                    txtSlot2Value.Enabled = true;
                    txtSlot3Value.Enabled = true;
                    txtSlot4Value.Enabled = true;
                    txtSlot5Value.Enabled = true;
                    txtSlot6Value.Enabled = true;
                    txtSlot7Value.Enabled = true;
                    txtSlot8Value.Enabled = true;
                    txtSlot9Value.Enabled = true;
                    txtSlot10Value.Enabled = true;
                    cbxSlot1Display.Enabled = true;
                    cbxSlot2Display.Enabled = true;
                    cbxSlot3Display.Enabled = true;
                    cbxSlot4Display.Enabled = true;
                    cbxSlot5Display.Enabled = true;
                    cbxSlot6Display.Enabled = true;
                    cbxSlot7Display.Enabled = true;
                    cbxSlot8Display.Enabled = true;
                    cbxSlot9Display.Enabled = true;
                    cbxSlot10Display.Enabled = true;
                }
                else
                {
                    throw new Exception("Failed to find inventory pointer. Your version of Resident Evil 3 is incompatible with this trainer.");
                }
            }
            ReadProcessMemory((int)processHandle, addressOfInventoryPointer, addressBuffer, 4, ref bytesRead);
            StartingAddressItem = BitConverter.ToInt32(addressBuffer, 0);
            if (StartingAddressItem == 0)
                return;
            StartingAddressValue = StartingAddressItem + 1;
            StartingAddressDisplay = StartingAddressValue + 1;
            ReadSlot(cbxSlot1Item, txtSlot1Value, cbxSlot1Display);
            ReadSlot(cbxSlot2Item, txtSlot2Value, cbxSlot2Display);
            ReadSlot(cbxSlot3Item, txtSlot3Value, cbxSlot3Display);
            ReadSlot(cbxSlot4Item, txtSlot4Value, cbxSlot4Display);
            ReadSlot(cbxSlot5Item, txtSlot5Value, cbxSlot5Display);
            ReadSlot(cbxSlot6Item, txtSlot6Value, cbxSlot6Display);
            ReadSlot(cbxSlot7Item, txtSlot7Value, cbxSlot7Display);
            ReadSlot(cbxSlot8Item, txtSlot8Value, cbxSlot8Display);
            ReadSlot(cbxSlot9Item, txtSlot9Value, cbxSlot9Display);
            ReadSlot(cbxSlot10Item, txtSlot10Value, cbxSlot10Display);
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.lblMusic.Text = music;
            about.Show();
        }

        private void ReadSlot(ComboBox item, TextBox value, ComboBox display)
        {
            byte[] buffer = new byte[3];
            int bytesRead = 0;
            item.SelectedIndexChanged -= ItemIndexChanged;
            value.TextChanged -= ValueChanged;
            display.SelectedIndexChanged -= DisplayIndexChanged;
            ReadProcessMemory((int)processHandle, StartingAddressItem + ((item.TabIndex / 3) * 4), buffer, 3, ref bytesRead);
            if (item.DroppedDown == false)
            {
                if (buffer[0] > 132)
                    buffer[0] = 0;
                item.SelectedValue = buffer[0];
            }
            if (value.Text != buffer[1].ToString())
            {
                value.Text = buffer[1].ToString();
            }
            if (display.DroppedDown == false)
            {
                display.SelectedValue = (byte)(buffer[2] % 16);
            }
            item.SelectedIndexChanged += ItemIndexChanged;
            value.TextChanged += ValueChanged;
            display.SelectedIndexChanged += DisplayIndexChanged;
        }

        private void ItemIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbxItem = (ComboBox)sender;
            int slotId = cbxItem.TabIndex / 3;
            byte item = (byte)cbxItem.SelectedValue;
            SetItem(slotId, item);
        }

        private void ValueChanged(object sender, EventArgs e)
        {
            TextBox txtValue = (TextBox)sender;
            if (txtValue.Text.Length == 0)
            {
                return;
            }
            if (Byte.TryParse(txtValue.Text, out byte value))
            {
                SetValue(txtValue.TabIndex / 3, value);
            }
            else
            {
                MessageBox.Show("Only enter numbers between 0 and 255", "Incorrect Input", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void DisplayIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbxDisplay = (ComboBox)sender;
            int slotId = cbxDisplay.TabIndex / 3;
            byte[] buffer = new byte[1];
            int bytesRead = 0;
            if (ReadProcessMemory((int)processHandle, StartingAddressItem + (slotId * 4), buffer, 1, ref bytesRead) && bytesRead == 1)
            {
                byte displayValue = (byte)cbxDisplay.SelectedValue;
                if (buffer[0] == 14 || buffer[0] == 15) //Check for Assault Rifle
                    displayValue |= 16; //Bitwise add the Auto/Manual flag to the value.
                SetDisplay(slotId, displayValue);
            }
        }

        private bool WriteBytes(byte[] value, int address)
        {
            if (processHandle == IntPtr.Zero)
                return false;
            int bytesWritten = 0;
            return (WriteProcessMemory((int)processHandle, address, value, value.Length, ref bytesWritten) == true && bytesWritten != 0);
        }

        private void SetItem(int slotId, Byte value)
        {
            if (!WriteBytes(new[] { value }, StartingAddressItem + (slotId * 4)))
                MessageBox.Show("Failed SetItem Slot: " + (slotId + 1) + " Item Value: " + value.ToString() + " Item: " + itemList[value], "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void SetValue(int slotId, Byte value)
        {
            if (!WriteBytes(new[] { value }, StartingAddressValue + (slotId * 4)))
                MessageBox.Show("Failed SetValue Slot: " + (slotId + 1) + " Value: " + value.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void SetDisplay(int slotId, Byte value)
        {
            if (!WriteBytes(new[] { value }, StartingAddressDisplay + (slotId * 4)))
                MessageBox.Show("Failed SetDisplay Slot: " + (slotId + 1) + " Display Value: " + value.ToString() + " Display: " + displayList[value], "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ReloadCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (ReloadByte == IntPtr.Zero)
            {
                if (cbkRReloads.Checked)
                {
                    cbkRReloads.Checked = false;
                    MessageBox.Show("Failed to find Reload address.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return;
            }
            if (cbkRReloads.Checked)
            {
                gHook.Hook();
            }
            else
            {
                gHook.UnHook();
            }
        }

        public void ReloadKeyDown(object sender, KeyEventArgs e)
        {
            if (!WriteBytes(new[] { (byte)0x00 }, ReloadByte.ToInt32()))
                MessageBox.Show("Failed to write Reload bytes Key Down.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void ReloadKeyUp(object sender, KeyEventArgs e)
        {
            if (!WriteBytes(new[] { (byte)0x06 }, ReloadByte.ToInt32()))
                MessageBox.Show("Failed to write Reload bytes Key Up.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void InkRibbonCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (InkRibbonAddress == IntPtr.Zero)
            {
                if (cbxInkRibbon.Checked)
                {
                    cbxInkRibbon.Checked = false;
                    MessageBox.Show("Failed to find Ink Ribbon address.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return;
            }
            if (!WriteBytes(new[] { (byte)(cbxInkRibbon.Checked == true ? 0x72 : 0x7C) }, InkRibbonAddress.ToInt32()))
                MessageBox.Show("Failed to write Ink Ribbon bytes.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void ValidateValueKeypress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (cbxInkRibbon.Checked)
            {
                WriteBytes(new[] { (byte)0x7C }, InkRibbonAddress.ToInt32());
            }
        }
    }
}
﻿namespace re3gir489
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.cbxSlot1Item = new System.Windows.Forms.ComboBox();
            this.cbxSlot1Display = new System.Windows.Forms.ComboBox();
            this.tmrMain = new System.Windows.Forms.Timer(this.components);
            this.lblWaiting = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbxSlot1 = new System.Windows.Forms.GroupBox();
            this.txtSlot1Value = new System.Windows.Forms.TextBox();
            this.gbxSlot2 = new System.Windows.Forms.GroupBox();
            this.txtSlot2Value = new System.Windows.Forms.TextBox();
            this.cbxSlot2Item = new System.Windows.Forms.ComboBox();
            this.cbxSlot2Display = new System.Windows.Forms.ComboBox();
            this.gbxSlot3 = new System.Windows.Forms.GroupBox();
            this.txtSlot3Value = new System.Windows.Forms.TextBox();
            this.cbxSlot3Item = new System.Windows.Forms.ComboBox();
            this.cbxSlot3Display = new System.Windows.Forms.ComboBox();
            this.gbxSlot4 = new System.Windows.Forms.GroupBox();
            this.txtSlot4Value = new System.Windows.Forms.TextBox();
            this.cbxSlot4Item = new System.Windows.Forms.ComboBox();
            this.cbxSlot4Display = new System.Windows.Forms.ComboBox();
            this.gbxSlot6 = new System.Windows.Forms.GroupBox();
            this.txtSlot6Value = new System.Windows.Forms.TextBox();
            this.cbxSlot6Item = new System.Windows.Forms.ComboBox();
            this.cbxSlot6Display = new System.Windows.Forms.ComboBox();
            this.gbxSlot5 = new System.Windows.Forms.GroupBox();
            this.txtSlot5Value = new System.Windows.Forms.TextBox();
            this.cbxSlot5Item = new System.Windows.Forms.ComboBox();
            this.cbxSlot5Display = new System.Windows.Forms.ComboBox();
            this.gbxSlot8 = new System.Windows.Forms.GroupBox();
            this.txtSlot8Value = new System.Windows.Forms.TextBox();
            this.cbxSlot8Item = new System.Windows.Forms.ComboBox();
            this.cbxSlot8Display = new System.Windows.Forms.ComboBox();
            this.gbxSlot7 = new System.Windows.Forms.GroupBox();
            this.txtSlot7Value = new System.Windows.Forms.TextBox();
            this.cbxSlot7Item = new System.Windows.Forms.ComboBox();
            this.cbxSlot7Display = new System.Windows.Forms.ComboBox();
            this.gbxSlot10 = new System.Windows.Forms.GroupBox();
            this.txtSlot10Value = new System.Windows.Forms.TextBox();
            this.cbxSlot10Item = new System.Windows.Forms.ComboBox();
            this.cbxSlot10Display = new System.Windows.Forms.ComboBox();
            this.gbxSlot9 = new System.Windows.Forms.GroupBox();
            this.txtSlot9Value = new System.Windows.Forms.TextBox();
            this.cbxSlot9Item = new System.Windows.Forms.ComboBox();
            this.cbxSlot9Display = new System.Windows.Forms.ComboBox();
            this.cbkRReloads = new System.Windows.Forms.CheckBox();
            this.cbxInkRibbon = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            this.gbxSlot1.SuspendLayout();
            this.gbxSlot2.SuspendLayout();
            this.gbxSlot3.SuspendLayout();
            this.gbxSlot4.SuspendLayout();
            this.gbxSlot6.SuspendLayout();
            this.gbxSlot5.SuspendLayout();
            this.gbxSlot8.SuspendLayout();
            this.gbxSlot7.SuspendLayout();
            this.gbxSlot10.SuspendLayout();
            this.gbxSlot9.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbxSlot1Item
            // 
            this.cbxSlot1Item.FormattingEnabled = true;
            this.cbxSlot1Item.Location = new System.Drawing.Point(6, 19);
            this.cbxSlot1Item.Name = "cbxSlot1Item";
            this.cbxSlot1Item.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot1Item.TabIndex = 0;
            this.cbxSlot1Item.SelectedIndexChanged += new System.EventHandler(this.ItemIndexChanged);
            // 
            // cbxSlot1Display
            // 
            this.cbxSlot1Display.FormattingEnabled = true;
            this.cbxSlot1Display.Location = new System.Drawing.Point(6, 73);
            this.cbxSlot1Display.Name = "cbxSlot1Display";
            this.cbxSlot1Display.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot1Display.TabIndex = 2;
            this.cbxSlot1Display.SelectedIndexChanged += new System.EventHandler(this.DisplayIndexChanged);
            // 
            // tmrMain
            // 
            this.tmrMain.Enabled = true;
            this.tmrMain.Interval = 500;
            this.tmrMain.Tick += new System.EventHandler(this.MainTimer_Tick);
            // 
            // lblWaiting
            // 
            this.lblWaiting.AutoSize = true;
            this.lblWaiting.BackColor = System.Drawing.Color.Transparent;
            this.lblWaiting.ForeColor = System.Drawing.Color.Red;
            this.lblWaiting.Location = new System.Drawing.Point(222, 596);
            this.lblWaiting.Name = "lblWaiting";
            this.lblWaiting.Size = new System.Drawing.Size(87, 13);
            this.lblWaiting.TabIndex = 2;
            this.lblWaiting.Text = "Waiting for game";
            this.lblWaiting.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(371, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // gbxSlot1
            // 
            this.gbxSlot1.BackColor = System.Drawing.Color.Transparent;
            this.gbxSlot1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gbxSlot1.Controls.Add(this.txtSlot1Value);
            this.gbxSlot1.Controls.Add(this.cbxSlot1Item);
            this.gbxSlot1.Controls.Add(this.cbxSlot1Display);
            this.gbxSlot1.ForeColor = System.Drawing.Color.White;
            this.gbxSlot1.Location = new System.Drawing.Point(32, 40);
            this.gbxSlot1.Name = "gbxSlot1";
            this.gbxSlot1.Size = new System.Drawing.Size(148, 101);
            this.gbxSlot1.TabIndex = 30;
            this.gbxSlot1.TabStop = false;
            this.gbxSlot1.Text = "Slot 1";
            // 
            // txtSlot1Value
            // 
            this.txtSlot1Value.Location = new System.Drawing.Point(7, 47);
            this.txtSlot1Value.MaxLength = 3;
            this.txtSlot1Value.Name = "txtSlot1Value";
            this.txtSlot1Value.Size = new System.Drawing.Size(44, 20);
            this.txtSlot1Value.TabIndex = 1;
            this.txtSlot1Value.Text = "0";
            this.txtSlot1Value.TextChanged += new System.EventHandler(this.ValueChanged);
            this.txtSlot1Value.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateValueKeypress);
            // 
            // gbxSlot2
            // 
            this.gbxSlot2.BackColor = System.Drawing.Color.Transparent;
            this.gbxSlot2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gbxSlot2.Controls.Add(this.txtSlot2Value);
            this.gbxSlot2.Controls.Add(this.cbxSlot2Item);
            this.gbxSlot2.Controls.Add(this.cbxSlot2Display);
            this.gbxSlot2.ForeColor = System.Drawing.Color.White;
            this.gbxSlot2.Location = new System.Drawing.Point(186, 40);
            this.gbxSlot2.Name = "gbxSlot2";
            this.gbxSlot2.Size = new System.Drawing.Size(148, 101);
            this.gbxSlot2.TabIndex = 31;
            this.gbxSlot2.TabStop = false;
            this.gbxSlot2.Text = "Slot 2";
            // 
            // txtSlot2Value
            // 
            this.txtSlot2Value.Location = new System.Drawing.Point(7, 47);
            this.txtSlot2Value.MaxLength = 3;
            this.txtSlot2Value.Name = "txtSlot2Value";
            this.txtSlot2Value.Size = new System.Drawing.Size(44, 20);
            this.txtSlot2Value.TabIndex = 4;
            this.txtSlot2Value.Text = "0";
            this.txtSlot2Value.TextChanged += new System.EventHandler(this.ValueChanged);
            this.txtSlot2Value.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateValueKeypress);
            // 
            // cbxSlot2Item
            // 
            this.cbxSlot2Item.FormattingEnabled = true;
            this.cbxSlot2Item.Location = new System.Drawing.Point(6, 19);
            this.cbxSlot2Item.Name = "cbxSlot2Item";
            this.cbxSlot2Item.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot2Item.TabIndex = 3;
            this.cbxSlot2Item.SelectedIndexChanged += new System.EventHandler(this.ItemIndexChanged);
            // 
            // cbxSlot2Display
            // 
            this.cbxSlot2Display.FormattingEnabled = true;
            this.cbxSlot2Display.Location = new System.Drawing.Point(6, 73);
            this.cbxSlot2Display.Name = "cbxSlot2Display";
            this.cbxSlot2Display.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot2Display.TabIndex = 5;
            this.cbxSlot2Display.SelectedIndexChanged += new System.EventHandler(this.DisplayIndexChanged);
            // 
            // gbxSlot3
            // 
            this.gbxSlot3.BackColor = System.Drawing.Color.Transparent;
            this.gbxSlot3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gbxSlot3.Controls.Add(this.txtSlot3Value);
            this.gbxSlot3.Controls.Add(this.cbxSlot3Item);
            this.gbxSlot3.Controls.Add(this.cbxSlot3Display);
            this.gbxSlot3.ForeColor = System.Drawing.Color.White;
            this.gbxSlot3.Location = new System.Drawing.Point(32, 147);
            this.gbxSlot3.Name = "gbxSlot3";
            this.gbxSlot3.Size = new System.Drawing.Size(148, 101);
            this.gbxSlot3.TabIndex = 32;
            this.gbxSlot3.TabStop = false;
            this.gbxSlot3.Text = "Slot 3";
            // 
            // txtSlot3Value
            // 
            this.txtSlot3Value.Location = new System.Drawing.Point(7, 47);
            this.txtSlot3Value.MaxLength = 3;
            this.txtSlot3Value.Name = "txtSlot3Value";
            this.txtSlot3Value.Size = new System.Drawing.Size(44, 20);
            this.txtSlot3Value.TabIndex = 7;
            this.txtSlot3Value.Text = "0";
            this.txtSlot3Value.TextChanged += new System.EventHandler(this.ValueChanged);
            this.txtSlot3Value.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateValueKeypress);
            // 
            // cbxSlot3Item
            // 
            this.cbxSlot3Item.FormattingEnabled = true;
            this.cbxSlot3Item.Location = new System.Drawing.Point(6, 19);
            this.cbxSlot3Item.Name = "cbxSlot3Item";
            this.cbxSlot3Item.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot3Item.TabIndex = 6;
            this.cbxSlot3Item.SelectedIndexChanged += new System.EventHandler(this.ItemIndexChanged);
            // 
            // cbxSlot3Display
            // 
            this.cbxSlot3Display.FormattingEnabled = true;
            this.cbxSlot3Display.Location = new System.Drawing.Point(6, 73);
            this.cbxSlot3Display.Name = "cbxSlot3Display";
            this.cbxSlot3Display.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot3Display.TabIndex = 8;
            this.cbxSlot3Display.SelectedIndexChanged += new System.EventHandler(this.DisplayIndexChanged);
            // 
            // gbxSlot4
            // 
            this.gbxSlot4.BackColor = System.Drawing.Color.Transparent;
            this.gbxSlot4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gbxSlot4.Controls.Add(this.txtSlot4Value);
            this.gbxSlot4.Controls.Add(this.cbxSlot4Item);
            this.gbxSlot4.Controls.Add(this.cbxSlot4Display);
            this.gbxSlot4.ForeColor = System.Drawing.Color.White;
            this.gbxSlot4.Location = new System.Drawing.Point(186, 147);
            this.gbxSlot4.Name = "gbxSlot4";
            this.gbxSlot4.Size = new System.Drawing.Size(148, 101);
            this.gbxSlot4.TabIndex = 33;
            this.gbxSlot4.TabStop = false;
            this.gbxSlot4.Text = "Slot 4";
            // 
            // txtSlot4Value
            // 
            this.txtSlot4Value.Location = new System.Drawing.Point(7, 47);
            this.txtSlot4Value.MaxLength = 3;
            this.txtSlot4Value.Name = "txtSlot4Value";
            this.txtSlot4Value.Size = new System.Drawing.Size(44, 20);
            this.txtSlot4Value.TabIndex = 10;
            this.txtSlot4Value.Text = "0";
            this.txtSlot4Value.TextChanged += new System.EventHandler(this.ValueChanged);
            this.txtSlot4Value.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateValueKeypress);
            // 
            // cbxSlot4Item
            // 
            this.cbxSlot4Item.FormattingEnabled = true;
            this.cbxSlot4Item.Location = new System.Drawing.Point(6, 19);
            this.cbxSlot4Item.Name = "cbxSlot4Item";
            this.cbxSlot4Item.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot4Item.TabIndex = 9;
            this.cbxSlot4Item.SelectedIndexChanged += new System.EventHandler(this.ItemIndexChanged);
            // 
            // cbxSlot4Display
            // 
            this.cbxSlot4Display.FormattingEnabled = true;
            this.cbxSlot4Display.Location = new System.Drawing.Point(6, 73);
            this.cbxSlot4Display.Name = "cbxSlot4Display";
            this.cbxSlot4Display.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot4Display.TabIndex = 11;
            this.cbxSlot4Display.SelectedIndexChanged += new System.EventHandler(this.DisplayIndexChanged);
            // 
            // gbxSlot6
            // 
            this.gbxSlot6.BackColor = System.Drawing.Color.Transparent;
            this.gbxSlot6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gbxSlot6.Controls.Add(this.txtSlot6Value);
            this.gbxSlot6.Controls.Add(this.cbxSlot6Item);
            this.gbxSlot6.Controls.Add(this.cbxSlot6Display);
            this.gbxSlot6.ForeColor = System.Drawing.Color.White;
            this.gbxSlot6.Location = new System.Drawing.Point(186, 254);
            this.gbxSlot6.Name = "gbxSlot6";
            this.gbxSlot6.Size = new System.Drawing.Size(148, 101);
            this.gbxSlot6.TabIndex = 35;
            this.gbxSlot6.TabStop = false;
            this.gbxSlot6.Text = "Slot 6";
            // 
            // txtSlot6Value
            // 
            this.txtSlot6Value.Location = new System.Drawing.Point(7, 47);
            this.txtSlot6Value.MaxLength = 3;
            this.txtSlot6Value.Name = "txtSlot6Value";
            this.txtSlot6Value.Size = new System.Drawing.Size(44, 20);
            this.txtSlot6Value.TabIndex = 16;
            this.txtSlot6Value.Text = "0";
            this.txtSlot6Value.TextChanged += new System.EventHandler(this.ValueChanged);
            this.txtSlot6Value.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateValueKeypress);
            // 
            // cbxSlot6Item
            // 
            this.cbxSlot6Item.FormattingEnabled = true;
            this.cbxSlot6Item.Location = new System.Drawing.Point(6, 19);
            this.cbxSlot6Item.Name = "cbxSlot6Item";
            this.cbxSlot6Item.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot6Item.TabIndex = 15;
            this.cbxSlot6Item.SelectedIndexChanged += new System.EventHandler(this.ItemIndexChanged);
            // 
            // cbxSlot6Display
            // 
            this.cbxSlot6Display.FormattingEnabled = true;
            this.cbxSlot6Display.Location = new System.Drawing.Point(6, 73);
            this.cbxSlot6Display.Name = "cbxSlot6Display";
            this.cbxSlot6Display.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot6Display.TabIndex = 17;
            this.cbxSlot6Display.SelectedIndexChanged += new System.EventHandler(this.DisplayIndexChanged);
            // 
            // gbxSlot5
            // 
            this.gbxSlot5.BackColor = System.Drawing.Color.Transparent;
            this.gbxSlot5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gbxSlot5.Controls.Add(this.txtSlot5Value);
            this.gbxSlot5.Controls.Add(this.cbxSlot5Item);
            this.gbxSlot5.Controls.Add(this.cbxSlot5Display);
            this.gbxSlot5.ForeColor = System.Drawing.Color.White;
            this.gbxSlot5.Location = new System.Drawing.Point(32, 254);
            this.gbxSlot5.Name = "gbxSlot5";
            this.gbxSlot5.Size = new System.Drawing.Size(148, 101);
            this.gbxSlot5.TabIndex = 34;
            this.gbxSlot5.TabStop = false;
            this.gbxSlot5.Text = "Slot 5";
            // 
            // txtSlot5Value
            // 
            this.txtSlot5Value.Location = new System.Drawing.Point(7, 47);
            this.txtSlot5Value.MaxLength = 3;
            this.txtSlot5Value.Name = "txtSlot5Value";
            this.txtSlot5Value.Size = new System.Drawing.Size(44, 20);
            this.txtSlot5Value.TabIndex = 13;
            this.txtSlot5Value.Text = "0";
            this.txtSlot5Value.TextChanged += new System.EventHandler(this.ValueChanged);
            this.txtSlot5Value.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateValueKeypress);
            // 
            // cbxSlot5Item
            // 
            this.cbxSlot5Item.FormattingEnabled = true;
            this.cbxSlot5Item.Location = new System.Drawing.Point(6, 19);
            this.cbxSlot5Item.Name = "cbxSlot5Item";
            this.cbxSlot5Item.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot5Item.TabIndex = 12;
            this.cbxSlot5Item.SelectedIndexChanged += new System.EventHandler(this.ItemIndexChanged);
            // 
            // cbxSlot5Display
            // 
            this.cbxSlot5Display.FormattingEnabled = true;
            this.cbxSlot5Display.Location = new System.Drawing.Point(6, 73);
            this.cbxSlot5Display.Name = "cbxSlot5Display";
            this.cbxSlot5Display.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot5Display.TabIndex = 14;
            this.cbxSlot5Display.SelectedIndexChanged += new System.EventHandler(this.DisplayIndexChanged);
            // 
            // gbxSlot8
            // 
            this.gbxSlot8.BackColor = System.Drawing.Color.Transparent;
            this.gbxSlot8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gbxSlot8.Controls.Add(this.txtSlot8Value);
            this.gbxSlot8.Controls.Add(this.cbxSlot8Item);
            this.gbxSlot8.Controls.Add(this.cbxSlot8Display);
            this.gbxSlot8.ForeColor = System.Drawing.Color.White;
            this.gbxSlot8.Location = new System.Drawing.Point(186, 361);
            this.gbxSlot8.Name = "gbxSlot8";
            this.gbxSlot8.Size = new System.Drawing.Size(148, 101);
            this.gbxSlot8.TabIndex = 37;
            this.gbxSlot8.TabStop = false;
            this.gbxSlot8.Text = "Slot 8";
            // 
            // txtSlot8Value
            // 
            this.txtSlot8Value.Location = new System.Drawing.Point(7, 47);
            this.txtSlot8Value.MaxLength = 3;
            this.txtSlot8Value.Name = "txtSlot8Value";
            this.txtSlot8Value.Size = new System.Drawing.Size(44, 20);
            this.txtSlot8Value.TabIndex = 22;
            this.txtSlot8Value.Text = "0";
            this.txtSlot8Value.TextChanged += new System.EventHandler(this.ValueChanged);
            this.txtSlot8Value.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateValueKeypress);
            // 
            // cbxSlot8Item
            // 
            this.cbxSlot8Item.FormattingEnabled = true;
            this.cbxSlot8Item.Location = new System.Drawing.Point(6, 19);
            this.cbxSlot8Item.Name = "cbxSlot8Item";
            this.cbxSlot8Item.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot8Item.TabIndex = 21;
            this.cbxSlot8Item.SelectedIndexChanged += new System.EventHandler(this.ItemIndexChanged);
            // 
            // cbxSlot8Display
            // 
            this.cbxSlot8Display.FormattingEnabled = true;
            this.cbxSlot8Display.Location = new System.Drawing.Point(6, 73);
            this.cbxSlot8Display.Name = "cbxSlot8Display";
            this.cbxSlot8Display.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot8Display.TabIndex = 23;
            this.cbxSlot8Display.SelectedIndexChanged += new System.EventHandler(this.DisplayIndexChanged);
            // 
            // gbxSlot7
            // 
            this.gbxSlot7.BackColor = System.Drawing.Color.Transparent;
            this.gbxSlot7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gbxSlot7.Controls.Add(this.txtSlot7Value);
            this.gbxSlot7.Controls.Add(this.cbxSlot7Item);
            this.gbxSlot7.Controls.Add(this.cbxSlot7Display);
            this.gbxSlot7.ForeColor = System.Drawing.Color.White;
            this.gbxSlot7.Location = new System.Drawing.Point(32, 361);
            this.gbxSlot7.Name = "gbxSlot7";
            this.gbxSlot7.Size = new System.Drawing.Size(148, 101);
            this.gbxSlot7.TabIndex = 36;
            this.gbxSlot7.TabStop = false;
            this.gbxSlot7.Text = "Slot 7";
            // 
            // txtSlot7Value
            // 
            this.txtSlot7Value.Location = new System.Drawing.Point(7, 47);
            this.txtSlot7Value.MaxLength = 3;
            this.txtSlot7Value.Name = "txtSlot7Value";
            this.txtSlot7Value.Size = new System.Drawing.Size(44, 20);
            this.txtSlot7Value.TabIndex = 19;
            this.txtSlot7Value.Text = "0";
            this.txtSlot7Value.TextChanged += new System.EventHandler(this.ValueChanged);
            this.txtSlot7Value.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateValueKeypress);
            // 
            // cbxSlot7Item
            // 
            this.cbxSlot7Item.FormattingEnabled = true;
            this.cbxSlot7Item.Location = new System.Drawing.Point(6, 19);
            this.cbxSlot7Item.Name = "cbxSlot7Item";
            this.cbxSlot7Item.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot7Item.TabIndex = 18;
            this.cbxSlot7Item.SelectedIndexChanged += new System.EventHandler(this.ItemIndexChanged);
            // 
            // cbxSlot7Display
            // 
            this.cbxSlot7Display.FormattingEnabled = true;
            this.cbxSlot7Display.Location = new System.Drawing.Point(6, 73);
            this.cbxSlot7Display.Name = "cbxSlot7Display";
            this.cbxSlot7Display.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot7Display.TabIndex = 20;
            this.cbxSlot7Display.SelectedIndexChanged += new System.EventHandler(this.DisplayIndexChanged);
            // 
            // gbxSlot10
            // 
            this.gbxSlot10.BackColor = System.Drawing.Color.Transparent;
            this.gbxSlot10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gbxSlot10.Controls.Add(this.txtSlot10Value);
            this.gbxSlot10.Controls.Add(this.cbxSlot10Item);
            this.gbxSlot10.Controls.Add(this.cbxSlot10Display);
            this.gbxSlot10.ForeColor = System.Drawing.Color.White;
            this.gbxSlot10.Location = new System.Drawing.Point(186, 468);
            this.gbxSlot10.Name = "gbxSlot10";
            this.gbxSlot10.Size = new System.Drawing.Size(148, 101);
            this.gbxSlot10.TabIndex = 39;
            this.gbxSlot10.TabStop = false;
            this.gbxSlot10.Text = "Slot 10";
            // 
            // txtSlot10Value
            // 
            this.txtSlot10Value.Location = new System.Drawing.Point(7, 47);
            this.txtSlot10Value.MaxLength = 3;
            this.txtSlot10Value.Name = "txtSlot10Value";
            this.txtSlot10Value.Size = new System.Drawing.Size(44, 20);
            this.txtSlot10Value.TabIndex = 28;
            this.txtSlot10Value.Text = "0";
            this.txtSlot10Value.TextChanged += new System.EventHandler(this.ValueChanged);
            this.txtSlot10Value.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateValueKeypress);
            // 
            // cbxSlot10Item
            // 
            this.cbxSlot10Item.FormattingEnabled = true;
            this.cbxSlot10Item.Location = new System.Drawing.Point(6, 19);
            this.cbxSlot10Item.Name = "cbxSlot10Item";
            this.cbxSlot10Item.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot10Item.TabIndex = 27;
            this.cbxSlot10Item.SelectedIndexChanged += new System.EventHandler(this.ItemIndexChanged);
            // 
            // cbxSlot10Display
            // 
            this.cbxSlot10Display.FormattingEnabled = true;
            this.cbxSlot10Display.Location = new System.Drawing.Point(6, 73);
            this.cbxSlot10Display.Name = "cbxSlot10Display";
            this.cbxSlot10Display.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot10Display.TabIndex = 29;
            this.cbxSlot10Display.SelectedIndexChanged += new System.EventHandler(this.DisplayIndexChanged);
            // 
            // gbxSlot9
            // 
            this.gbxSlot9.BackColor = System.Drawing.Color.Transparent;
            this.gbxSlot9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gbxSlot9.Controls.Add(this.txtSlot9Value);
            this.gbxSlot9.Controls.Add(this.cbxSlot9Item);
            this.gbxSlot9.Controls.Add(this.cbxSlot9Display);
            this.gbxSlot9.ForeColor = System.Drawing.Color.White;
            this.gbxSlot9.Location = new System.Drawing.Point(32, 468);
            this.gbxSlot9.Name = "gbxSlot9";
            this.gbxSlot9.Size = new System.Drawing.Size(148, 101);
            this.gbxSlot9.TabIndex = 38;
            this.gbxSlot9.TabStop = false;
            this.gbxSlot9.Text = "Slot 9";
            // 
            // txtSlot9Value
            // 
            this.txtSlot9Value.Location = new System.Drawing.Point(7, 47);
            this.txtSlot9Value.MaxLength = 3;
            this.txtSlot9Value.Name = "txtSlot9Value";
            this.txtSlot9Value.Size = new System.Drawing.Size(44, 20);
            this.txtSlot9Value.TabIndex = 25;
            this.txtSlot9Value.Text = "0";
            this.txtSlot9Value.TextChanged += new System.EventHandler(this.ValueChanged);
            this.txtSlot9Value.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateValueKeypress);
            // 
            // cbxSlot9Item
            // 
            this.cbxSlot9Item.FormattingEnabled = true;
            this.cbxSlot9Item.Location = new System.Drawing.Point(6, 19);
            this.cbxSlot9Item.Name = "cbxSlot9Item";
            this.cbxSlot9Item.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot9Item.TabIndex = 24;
            this.cbxSlot9Item.SelectedIndexChanged += new System.EventHandler(this.ItemIndexChanged);
            // 
            // cbxSlot9Display
            // 
            this.cbxSlot9Display.FormattingEnabled = true;
            this.cbxSlot9Display.Location = new System.Drawing.Point(6, 73);
            this.cbxSlot9Display.Name = "cbxSlot9Display";
            this.cbxSlot9Display.Size = new System.Drawing.Size(137, 21);
            this.cbxSlot9Display.TabIndex = 26;
            this.cbxSlot9Display.SelectedIndexChanged += new System.EventHandler(this.DisplayIndexChanged);
            // 
            // cbkRReloads
            // 
            this.cbkRReloads.AutoSize = true;
            this.cbkRReloads.BackColor = System.Drawing.Color.Black;
            this.cbkRReloads.ForeColor = System.Drawing.Color.White;
            this.cbkRReloads.Location = new System.Drawing.Point(12, 584);
            this.cbkRReloads.Name = "cbkRReloads";
            this.cbkRReloads.Size = new System.Drawing.Size(189, 17);
            this.cbkRReloads.TabIndex = 13;
            this.cbkRReloads.Text = "Holding R Reloads When You Fire";
            this.cbkRReloads.UseVisualStyleBackColor = false;
            this.cbkRReloads.CheckedChanged += new System.EventHandler(this.ReloadCheckbox_CheckedChanged);
            // 
            // cbxInkRibbon
            // 
            this.cbxInkRibbon.AutoSize = true;
            this.cbxInkRibbon.BackColor = System.Drawing.Color.Black;
            this.cbxInkRibbon.ForeColor = System.Drawing.Color.White;
            this.cbxInkRibbon.Location = new System.Drawing.Point(12, 607);
            this.cbxInkRibbon.Name = "cbxInkRibbon";
            this.cbxInkRibbon.Size = new System.Drawing.Size(185, 17);
            this.cbxInkRibbon.TabIndex = 14;
            this.cbxInkRibbon.Text = "No Ink Ribbon Required To Save";
            this.cbxInkRibbon.UseVisualStyleBackColor = false;
            this.cbxInkRibbon.CheckedChanged += new System.EventHandler(this.InkRibbonCheckbox_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.BackgroundImage = global::re3gir489.Properties.Resources.Untitled;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(371, 628);
            this.Controls.Add(this.cbxInkRibbon);
            this.Controls.Add(this.cbkRReloads);
            this.Controls.Add(this.gbxSlot10);
            this.Controls.Add(this.gbxSlot9);
            this.Controls.Add(this.gbxSlot8);
            this.Controls.Add(this.gbxSlot7);
            this.Controls.Add(this.gbxSlot6);
            this.Controls.Add(this.gbxSlot4);
            this.Controls.Add(this.gbxSlot5);
            this.Controls.Add(this.gbxSlot3);
            this.Controls.Add(this.gbxSlot2);
            this.Controls.Add(this.gbxSlot1);
            this.Controls.Add(this.lblWaiting);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Resident Evil 3 Inventory Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.gbxSlot1.ResumeLayout(false);
            this.gbxSlot1.PerformLayout();
            this.gbxSlot2.ResumeLayout(false);
            this.gbxSlot2.PerformLayout();
            this.gbxSlot3.ResumeLayout(false);
            this.gbxSlot3.PerformLayout();
            this.gbxSlot4.ResumeLayout(false);
            this.gbxSlot4.PerformLayout();
            this.gbxSlot6.ResumeLayout(false);
            this.gbxSlot6.PerformLayout();
            this.gbxSlot5.ResumeLayout(false);
            this.gbxSlot5.PerformLayout();
            this.gbxSlot8.ResumeLayout(false);
            this.gbxSlot8.PerformLayout();
            this.gbxSlot7.ResumeLayout(false);
            this.gbxSlot7.PerformLayout();
            this.gbxSlot10.ResumeLayout(false);
            this.gbxSlot10.PerformLayout();
            this.gbxSlot9.ResumeLayout(false);
            this.gbxSlot9.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxSlot1Item;
        private System.Windows.Forms.ComboBox cbxSlot1Display;
        private System.Windows.Forms.Timer tmrMain;
        private System.Windows.Forms.Label lblWaiting;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbxSlot1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TextBox txtSlot1Value;
        private System.Windows.Forms.GroupBox gbxSlot2;
        private System.Windows.Forms.TextBox txtSlot2Value;
        private System.Windows.Forms.ComboBox cbxSlot2Item;
        private System.Windows.Forms.ComboBox cbxSlot2Display;
        private System.Windows.Forms.GroupBox gbxSlot3;
        private System.Windows.Forms.TextBox txtSlot3Value;
        private System.Windows.Forms.ComboBox cbxSlot3Item;
        private System.Windows.Forms.ComboBox cbxSlot3Display;
        private System.Windows.Forms.GroupBox gbxSlot4;
        private System.Windows.Forms.TextBox txtSlot4Value;
        private System.Windows.Forms.ComboBox cbxSlot4Item;
        private System.Windows.Forms.ComboBox cbxSlot4Display;
        private System.Windows.Forms.GroupBox gbxSlot6;
        private System.Windows.Forms.TextBox txtSlot6Value;
        private System.Windows.Forms.ComboBox cbxSlot6Item;
        private System.Windows.Forms.ComboBox cbxSlot6Display;
        private System.Windows.Forms.GroupBox gbxSlot5;
        private System.Windows.Forms.TextBox txtSlot5Value;
        private System.Windows.Forms.ComboBox cbxSlot5Item;
        private System.Windows.Forms.ComboBox cbxSlot5Display;
        private System.Windows.Forms.GroupBox gbxSlot8;
        private System.Windows.Forms.TextBox txtSlot8Value;
        private System.Windows.Forms.ComboBox cbxSlot8Item;
        private System.Windows.Forms.ComboBox cbxSlot8Display;
        private System.Windows.Forms.GroupBox gbxSlot7;
        private System.Windows.Forms.TextBox txtSlot7Value;
        private System.Windows.Forms.ComboBox cbxSlot7Item;
        private System.Windows.Forms.ComboBox cbxSlot7Display;
        private System.Windows.Forms.GroupBox gbxSlot10;
        private System.Windows.Forms.TextBox txtSlot10Value;
        private System.Windows.Forms.ComboBox cbxSlot10Item;
        private System.Windows.Forms.ComboBox cbxSlot10Display;
        private System.Windows.Forms.GroupBox gbxSlot9;
        private System.Windows.Forms.TextBox txtSlot9Value;
        private System.Windows.Forms.ComboBox cbxSlot9Item;
        private System.Windows.Forms.ComboBox cbxSlot9Display;
        private System.Windows.Forms.CheckBox cbkRReloads;
        private System.Windows.Forms.CheckBox cbxInkRibbon;
    }
}

